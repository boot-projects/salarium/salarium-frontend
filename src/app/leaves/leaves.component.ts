import { Component } from '@angular/core';
import { LeavesService } from '../service/leaves.service';
import { DatePipe } from '@angular/common';
import { ILeaves, Leaves } from '../../classes/leaves';

@Component({
  selector: 'app-leaves',
  templateUrl: './leaves.component.html',
  styleUrl: './leaves.component.css'
})
export class LeavesComponent {
  leavesArray: ILeaves[] = [];
  leavesObj: Leaves = new Leaves();

  resetObj(): void {
    this.leavesObj = new Leaves();
  }

  constructor(private leavesService: LeavesService) {
    this.resetObj();
  }

  ngOnInit(): void {
    this.loadAllLeaves();
  }

  loadAllLeaves(): void {
    this.leavesService.getAllLeaves().subscribe((res: any) => {
      this.leavesArray = res;
    }
  );
  }

  onSave(): void {
    this.leavesService.createLeave(this.leavesObj).subscribe((res: any) => {
      this.loadAllLeaves();
      alert(res);
    });
  }

  onUpdate(): void {
    this.leavesService.updateLeave(this.leavesObj, this.leavesObj.leaveId).subscribe((res: any) => {
      this.loadAllLeaves();
      alert(res);
    });
  }

  onReset(): void {
    this.resetObj();
  }

  onEdit(id: number): void {
    this.leavesService.getLeavebyId(id).subscribe((res: any) => {
      this.leavesObj = res;
    });
  }

  onDelete(id : number): void {
    this.leavesService.deleteLeave(this.leavesObj.leaveId).subscribe((res: any)=>{
      alert(res);
      this.loadAllLeaves();
    });
  }
}
