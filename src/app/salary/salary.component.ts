import { SalaryService } from './../service/salary.service';
import { Component, OnInit } from '@angular/core';
import { ISalary, Salary } from '../../classes/salary';

@Component({
  selector: 'app-salary',
  templateUrl: './salary.component.html',
  styleUrl: './salary.component.css'
})
export class SalaryComponent implements OnInit {
  salaryObj: Salary = new Salary();
  salaryArray: ISalary[] = [];
  totalAdvAmount: number = 0;
  totalLeaves: number = 0;
  employeeArray: any[] = [];

  resetObj() {
    this.salaryObj = new Salary();
  }

  constructor(private salaryService : SalaryService) {  }

  ngOnInit(): void {
    // this.loadAllEmployees();
    this.loadAllSalary();
  }

  // loadAllEmployees(): void {
  //    this.employeeService.getAllEmployees().subscribe((res: any) => {
  //      this.employeeArray = res;
  //   });
  // }

  loadAllSalary(): void {
    this.salaryService.getSalaries().subscribe((res: any) => {
      this.salaryArray = res;
    });
  }

  // getAllAdvance() {
  //   this.http.get("https://onlinetestapi.gerasim.in/getAllAdvance").subscribe((res: any) => {
  //     const data = res.data.filter((m: any) => m.employeeId == this.salaryObj.empId)

  //     data.foreach((m: any) => {
  //       this.totalAdvAmount += m.AdvanceAmount;
  //     })
  //     this.salaryObj.totalAdvance = this.totalAdvAmount;
  //   })
  // }

  // getAllLeaves() {
  //   this.http.get("https://onlinetestapi.gerasim.in/getAllLeaves").subscribe((res: any) => {
  //     this.totalLeaves = res.data.filter((m: any) => {
  //       m.employeeId==this.salaryObj.empId
  //     }).length

  //     this.salaryObj.presentDays = 30 - this.totalLeaves;
  //   })
  // }

  calculateSalary() {
    // const data = this.employeeArray.find((e: any) => e.empId == this.salaryObj.empId);

    const perDaysalary = this.salaryObj.salaryAmount / 30;
    this.salaryObj.salaryToBePaid = (this.salaryObj.presentDays * perDaysalary)- this.salaryObj.totalAdvance;    // + perDaysalary * totalLeaves (fulldayLeaves + 1/2 * halfDayLeaves)   have to make it for a month
  }

  onEdit(salaryId:number): void {
    this.salaryService.getSalarybyId(salaryId).subscribe((res: any) => {
      this.salaryObj = res.data;
    })
  }

  onSave(): void {
    this.salaryService.createsalary(this.salaryObj).subscribe(
      (res: any) => {
        this.loadAllSalary();
        alert(res);
      }
  )}

  onDelete(id: number): void {
    this.salaryService.deleteSalary(id).subscribe((res: any) => {
      alert(res)
      this.loadAllSalary(); // update
    });
  }

  onUpdate(): void {
    this.salaryService.updateSalary(this.salaryObj.empId, this.salaryObj).subscribe(
      (res: any) => {
        this.loadAllSalary();
      }
    )
  }

  onReset(): void {
    this.resetObj();
  }


}
