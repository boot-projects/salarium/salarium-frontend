import { Component } from '@angular/core';
import { AdvanceService } from '../service/advance.service';
import { Advance, IAdvance } from '../../classes/advance';

@Component({
  selector: 'app-advance',
  templateUrl: './advance.component.html',
  styleUrl: './advance.component.css'
})
export class AdvanceComponent {
  advanceArray: IAdvance[] = [];
  advanceObj: Advance = new Advance();

  resetObj() {
    this.advanceObj = new Advance();
  }

  constructor(private advanceService : AdvanceService) {
    this.resetObj();
   }

  ngOnInit(): void {
    this.loadAllAdvances();
  }

  loadAllAdvances() : void {
    this.advanceService.getAllAdvances().subscribe((res:any)=> {
      this.advanceArray = res;
    })
  }

  onSave(): void {
    this.advanceService.createAdvance(this.advanceObj).subscribe(
      (res: any) => {
        this.loadAllAdvances();
        alert(res);
      },
      (err: any) => {
        alert(err);
        this.loadAllAdvances();
      }
  )}

  onUpdate(): void {
    this.advanceService.updateAdvance(this.advanceObj.empId, this.advanceObj).subscribe(
      (res: any) => {
        this.loadAllAdvances();
        // alert(res);

        if (res.id != 0) {  //empId
          alert("updated employee");
        }
      }
  )}

  onReset() {
    this.resetObj();
  }

  onEdit(id: number): void {
    this.advanceService.getAdvancebyId(id).subscribe((res: any) => {
      this.advanceObj = res;
    })
  }

  onDelete(id: number): void {
    this.advanceService.deleteAdvance(id).subscribe((res: any) => {
      alert(res)
      this.loadAllAdvances();
    });
  }
}
