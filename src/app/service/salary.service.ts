import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalaryService {
  apiBaseUrl: string = "http://localhost:8080/api/v1/salaries"

  constructor(private http: HttpClient) { }

  getSalaries(): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}`);
  }

  createsalary(obj: any): Observable<any> {
    return this.http.post(`${this.apiBaseUrl}`, obj);
  }

  getSalarybyId(id: number): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}/${id}`);
  }
  getSalarybyEmpId(id: number): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}/employees/${id}`);
  }

  updateSalary(id: number, obj: any): Observable<any> {
    return this.http.put(`${this.apiBaseUrl}/${id}`, obj);
  }

  deleteSalary(id: number): Observable<any> {
    return this.http.delete(`${this.apiBaseUrl}/${id}`);
  }
}
