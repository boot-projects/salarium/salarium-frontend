import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {
  apiBaseUrl: string = "http://localhost:8080/api/v1/attendance"

  constructor(private http: HttpClient) { }

  getAttendance(): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}`);
  }

  createAttendance(attendanceObj: any) : Observable<any>{
    return this.http.post(`${this.apiBaseUrl}`, attendanceObj);
  }

  getAttendancebyId(attendanceId: number): Observable<any>{
    return this.http.get(`${this.apiBaseUrl}/${attendanceId}`);
  }

  getAttendancebyEmpId(empId: number): Observable<any>{
    return this.http.get(`${this.apiBaseUrl}/${empId}`);
  }

  updateAttendance(id: number, attendanceObj : any) : Observable<any>{
    return this.http.put(`${this.apiBaseUrl}/${id}`,attendanceObj);
  }

  deleteAttendance(id: number): Observable<any>{
    return this.http.delete(`${this.apiBaseUrl}/${id}`);
  }
}
