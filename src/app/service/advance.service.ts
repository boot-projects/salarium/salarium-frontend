import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdvanceService {
  apiBaseUrl: string = "http://localhost:8080/api/v1/advances"

  constructor(private http: HttpClient) { }

  getAllAdvances(): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}`);
  }

  createAdvance(obj: any) : Observable<any>{
    return this.http.post(`${this.apiBaseUrl}`, obj);
  }

  getAdvancebyId(id: number): Observable<any>{
    return this.http.get(`${this.apiBaseUrl}/${id}`);
  }

  getAdvancebyEmpId(empId: number): Observable<any>{
    return this.http.get(`${this.apiBaseUrl}/${empId}`);
  }

  updateAdvance(id: number, obj : any) : Observable<any>{
    return this.http.put(`${this.apiBaseUrl}/${id}`,obj);
  }

  deleteAdvance(id: number): Observable<any>{
    return this.http.delete(`${this.apiBaseUrl}/${id}`);
  }
}
