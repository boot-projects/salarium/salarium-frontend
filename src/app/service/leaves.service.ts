import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LeavesService {
  apiBaseUrl: string = "http://localhost:8080/api/v1/leaves"

  constructor(private http: HttpClient) { }

  getAllLeaves(): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}`);
  }

  createLeave(obj: any): Observable<any> {
    return this.http.post(`${this.apiBaseUrl}`, obj);
  }

  getLeavebyId(id: number): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}/${id}`);
  }
  getLeavebyEmpId(id: number): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}/${id}`);
  }

  updateLeave(obj: any, id: number): Observable<any> {
    return this.http.put(`${this.apiBaseUrl}/${id}`, obj);
  }

  deleteLeave(id: number): Observable<any> {
    return this.http.delete(`${this.apiBaseUrl}/${id}`);
  }
}
