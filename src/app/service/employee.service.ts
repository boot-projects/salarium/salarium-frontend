import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  apiBaseUrl: string = "http://localhost:8080/api/v1/employees"

  constructor(private http: HttpClient) { }

  getAllEmployees(): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}`);
  }

  createEmployee(obj: any) : Observable<any>{
    return this.http.post(`${this.apiBaseUrl}`, obj);
  }

  getEmployeebyId(id: number): Observable<any>{
    return this.http.get(`${this.apiBaseUrl}/${id}`);
  }

  updateEmployee(obj : any, id: number) : Observable<any>{
    return this.http.put(`${this.apiBaseUrl}/${id}`,obj);
  }

  deleteEmployee(id: number): Observable<any>{
    return this.http.delete(`${this.apiBaseUrl}/${id}`);
  }
}
