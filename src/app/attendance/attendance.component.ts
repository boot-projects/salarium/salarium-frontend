import { Attendance } from './../../../../salarium-frontend/salarium/src/classes/employee';
import { AttendanceService } from './../service/attendance.service';
import { Component, OnInit } from '@angular/core';
import { Attendee, IAttendance } from '../../classes/attendee';
import { EmployeeService } from '../service/employee.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrl: './attendance.component.css'
})
export class AttendanceComponent implements OnInit {
  attendanceArray: IAttendance [] = [];
  attendanceObj: Attendee = new Attendee();
  employeeArray: any[] = [];

  resetObj() {
    this.attendanceObj = new Attendee();
  }

  constructor(private attendanceService: AttendanceService) {
    this.resetObj();
  }

  ngOnInit(): void {
    this.loadAllAttendance();
  }

  loadAllAttendance() {
    return this.attendanceService.getAttendance().subscribe((response: any) => {
      this.attendanceArray = response;
    });
  }

  onEdit(attendanceId:number):void {
    this.attendanceService.getAttendancebyId(attendanceId).subscribe((response: any) => {
      this.attendanceObj = response;
    })
  }

  onDelete(attendanceId:number):void  {
    this.attendanceService.deleteAttendance(attendanceId).subscribe((response: any) => {
      alert(response);
    },
      (error: any) => {
      alert(error);
    })
  }

  onReset() {
    this.resetObj();
  }

  onSave() {
    this.attendanceService.createAttendance(this.attendanceObj).subscribe((response: any) => {
      this.loadAllAttendance();
      alert(response.message);
    },
      (error: any) => {
        alert(error.message);

      }
    );
  }

  onUpdate() {
    this.attendanceService.updateAttendance(this.attendanceObj.attendanceId,this.attendanceObj).subscribe((response: any) => {
      this.loadAllAttendance();
      alert(response.message);
    },
      (error: any) => {
        alert(error.message);
      }
    );
  }


}
