import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { LeavesComponent } from './leaves/leaves.component';
import { EmployeeComponent } from './employee/employee.component';
import { AdvanceComponent } from './advance/advance.component';
import { SalaryComponent } from './salary/salary.component';
import { authguardGuard } from './guards/authguard.guard';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [authguardGuard]
      },
      {
        path: 'employee',
        component: EmployeeComponent,
        canActivate: [authguardGuard]
      },
      {
        path: 'attendance',
        component: AttendanceComponent,
        canActivate: [authguardGuard]
      },
      {
        path: 'leaves',
        component: LeavesComponent,
        canActivate: [authguardGuard]
      },
      {
        path: 'advance',
        component: AdvanceComponent,
        canActivate: [authguardGuard]
      },
      {
        path: 'salary',
        component: SalaryComponent,
        canActivate: [authguardGuard]
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
