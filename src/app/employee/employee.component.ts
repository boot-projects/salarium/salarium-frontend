import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../service/employee.service';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrl: './employee.component.css'
})
export class EmployeeComponent implements OnInit {

  employeeArray: any[] = [];

  employeeObj: any;

  resetObj() {
    this.employeeObj =
    {
      "empId": 0,
      "empName": "",
      "empContactNo": "",
      "empAltContactNo": "",
      "empEmail": "",
      "addressLine1": "",
      "addressLine2": "",
      "pincode": "",
      "city": "",
      "state": "",
      "bankName": "",
      "ifsc": "",
      "accountNo": "",
      "bankBranch": "",
      "salary": 0
    }
  }

  constructor(private employeeService: EmployeeService) {
    this.resetObj();
   }

  ngOnInit(): void {
    this.loadAllEmployees();
  }

  loadAllEmployees() : void {
    this.employeeService.getAllEmployees().subscribe((res:any)=> {
      this.employeeArray = res;
    })
  }

  onSave(): void {
    this.employeeService.createEmployee(this.employeeObj).subscribe(
      (res: any) => {
        this.loadAllEmployees();
        alert(res);

        // if (res.success) {
        //   alert(res.message);
        // }
      }
  )}

  onUpdate(): void {
    this.employeeService.updateEmployee(this.employeeObj,this.employeeObj.empId).subscribe(
      (res: any) => {
        this.loadAllEmployees();
        // alert(res);

        if (res.empId != 0) {  //empId
          alert("updated employee");
        }
        // if (res.success) {
        //   alert(res.message);
        // }
      }
  )}

  onReset() {
    // this.resetObj;
    this.resetObj();
  }

  onEdit(empId: number): void {
    this.employeeService.getEmployeebyId(empId).subscribe((res: any) => {
      this.employeeObj = res;
    })
  }

  onDelete(empId: number): void {
    this.employeeService.deleteEmployee(empId).subscribe((res: any) => {
      alert("successfully Deleted");
      this.loadAllEmployees(); // update
    });
  }
}
