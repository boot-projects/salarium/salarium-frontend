import { LoginService } from './../service/login.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  UserObj: any = {
    email: '',
    password: ''
  };

  constructor(private router: Router, private loginService : LoginService) {

  }

  onLogin() {
    // if (this.UserObj.userName == "Admin" && this.UserObj.password == "admin@123") {
    //   this.router.navigateByUrl('dashboard');
    // } else {
    //   alert('UserName or password not found');
    // }

    this.loginService.login(this.UserObj).subscribe((res: any) => {
      if (res.token != null) {
        localStorage.setItem('token', res.token);
        alert('User Successfully logged in');
        this.router.navigate(['/dashboard']);
      }else
        alert('User Failure');
    });
  }
}
