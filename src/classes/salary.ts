export interface ISalary {
  salaryId: number,
  empId: number,
  salaryDate: Date,
  totalAdvance: number,
  presentDays: number,
  totalLeaves: number,
  salaryAmount: number,
  salaryToBePaid: number
}

export class Salary{
  salaryId: number
  empId: number
  salaryDate?: Date
  totalAdvance: number
  presentDays: number
  totalLeaves: number
  salaryAmount: number
  salaryToBePaid: number


  constructor() {
    this.salaryId = 0;
    this.empId = 0;
    this.salaryDate = undefined;
    this.totalAdvance = 0;
    this.presentDays = 0;
    this.totalLeaves = 0;
    this.salaryAmount = 0;
    this.salaryToBePaid = 0;
  }
}
