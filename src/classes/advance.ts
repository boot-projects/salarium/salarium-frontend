export interface IAdvance {
  advanceId: number,
  empId: number,
  advanceDate: Date,
  advanceAmount: number,
  reason: string
}

export class Advance{
  advanceId: number
  empId: number
  advanceDate?: Date
  advanceAmount: number
  reason: string

  constructor() {
    this.advanceId = 0;
    this.empId = 0;
    this.advanceDate = undefined;
    this.advanceAmount = 0;
    this.reason = '';
  }
}
