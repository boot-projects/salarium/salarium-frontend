export interface IAttendance {
  empName: string
  empContactNo: string
  empId: number
  attendanceDate: string
  attendanceId: number
  inTime: string
  outTime: string
  isFullDay: boolean
}

export class Attendee{
  attendanceId: number
  empId: number
  attendanceDate?: Date
  inTime?: Date
  outTime?: Date
  isFullDay: boolean

  constructor() {
    this.attendanceId = 0;
    this.empId = 0;
    this.attendanceDate = undefined;
    this.inTime = undefined;
    this.outTime = undefined;
    this.isFullDay = false;
  }
}
