export interface ILeaves {
  leaveId: number,
  employeeId: number,
  leaveDate: Date,
  leaveReason: string,
  noOfFullDayLeaves: number,
  noOfHalfDayLeaves: number,
  empId: number
}

export class Leaves{
  leaveId: number
  empId: number
  leaveDate?: Date
  leaveReason: string
  noOfFullDayLeaves: number
  noOfHalfDayLeaves: number

  constructor() {
    this.leaveId = 0;
    this.empId = 0;
    this.leaveDate = undefined;
    this.leaveReason = '';
    this.noOfFullDayLeaves = 0;
    this.noOfHalfDayLeaves = 0;
  }
}
